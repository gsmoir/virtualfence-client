This is the android client application for VirtualFence project.

VirtualFence is an application to geofence children and elderly people. If the children or elderly people go out of the geofence set by the caregiver, the caregivers are notified, and also alerting the children or elderly person. A cared person(child or elderly) can be monitored by multiple caregivers, and also a caregiver can monitor multiple cared persons.

The project currently makes use of android devices, GCM server and a MEAN stack server. Android device's GPS technology is used for location tracking, and GCM server along with a 3rd party server is used as a communication bridge between the devices.

This project is in continuation of the former project "Virtual-fence", eliminating the app-to-GCM direct communication by introducing an extra 3rd party server in MEAN stack.