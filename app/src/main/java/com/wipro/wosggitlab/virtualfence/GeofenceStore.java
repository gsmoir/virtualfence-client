package com.wipro.wosggitlab.virtualfence;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

/**
 * Created by sam on 07-08-2015.
 *
 *
 */
public class GeofenceStore implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<Status>, LocationListener {

    private final String TAG = "Sam";//this.getClass().getSimpleName();

    private Context context;

    //Google API client object
    private GoogleApiClient googleApiClient;

    //Geofence Pending Intent
    private PendingIntent pendingIntent;

    //List of Geofences to monitor
    private ArrayList<Geofence> arraylist_geofences;

    //Geofence Request
    private GeofencingRequest geofencingRequest;

    //Request Object
    private LocationRequest locationRequest;

    /**
     * Constructor
     * @param ctx
     * @param geofences
     */
    public GeofenceStore(Context ctx, ArrayList<Geofence> geofences) {

        Log.e(TAG, "Inside GeofenceStore constructor....");

        context = ctx;
        arraylist_geofences = new ArrayList<Geofence>(geofences);
        pendingIntent = null;

        Log.e(TAG, "GeofenceStore --- 1");
        /**
         * Build an GoogleApiClient, specifying the need for LocationServices, by adding the API to client
         * Specify connection callbacks, onConnectionFailed methods are in this class
         */
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        googleApiClient.connect();

        Log.e(TAG, "GeofenceStore --- 2");
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e(TAG, "GeofenceStore --- 3");
        //Create Geofencing request
        geofencingRequest = new GeofencingRequest.Builder().addGeofences(arraylist_geofences).build();
        Log.e(TAG, "GeofenceStore --- 4");
        pendingIntent = createRequestPendingIntent();
        Log.e(TAG, "GeofenceStore --- 5");
        //Submitting request to monitoring geofences
        PendingResult<Status> pendingResult = null;
        try {
            pendingResult = LocationServices.GeofencingApi.addGeofences(googleApiClient, geofencingRequest, pendingIntent);
            Log.e(TAG, "GeofenceStore --- 6");
            pendingResult.setResultCallback(this);

        } catch(SecurityException securityException) {
        // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            Log.e(TAG, "ACCESS_FINE_LOCATION exception caused here....");
        }
        Log.e(TAG, "GeofenceStore --- 7");
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.v(TAG, "GS - Suspended connection");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG, "GS - Location Information\n"
                + "==========\n"
                + "Provider:\t" + location.getProvider() + "\n"
                + "Lat & Long:\t" + location.getLatitude() + ", "
                + location.getLongitude() + "\n"
                + "Altitude:\t" + location.getAltitude() + "\n"
                + "Bearing:\t" + location.getBearing() + "\n"
                + "Speed:\t\t" + location.getSpeed() + "\n"
                + "Accuracy:\t" + location.getAccuracy() + "\n");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v(TAG, "GS - Connection failed.");
    }

    @Override
    public void onResult(Status status) {
        if(status.isSuccess()) {
            Log.e(TAG, "GS - Geofencing success");
        } else if (status.isCanceled()) {
            Log.e(TAG, "GS - Canceled");
        } else if (status.isInterrupted()) {
            Log.e(TAG, "GS - Interrupted");
        }
    }

    /**
     * This creates a pendingIntent that is to be fired when geofence transitions takes place.
     * In this instance , we are using an InstanceService to handle the services.
     */
    private PendingIntent createRequestPendingIntent(){
        Log.e(TAG, "pendingIntent"+ pendingIntent);
        if(pendingIntent == null) {

            Log.e(TAG, "CreatePending intent - 1");
            Intent intent = new Intent(context, GeofenceIntentService.class);
            Log.e(TAG, "CreatePending intent - 2" +"Intent = " +intent );
            pendingIntent = PendingIntent.getService(context,0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Log.e(TAG, "CreatePending intent- 3 " + pendingIntent);
        }
        return pendingIntent;
    }
}
