package com.wipro.wosggitlab.virtualfence;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.wipro.wosggitlab.virtualfence.gcmServer.PostToGcm;

import java.util.List;

/**
 * Created by sam on 08-08-2015.
 *
 * Identifies Geofence changes and notifies itself
 * Also, sends GCM message using the PostToGcm class
 */
public class GeofenceIntentService extends IntentService{


    private String TAG = "Sam";

    public GeofenceIntentService() {
        super("GeofenceIntentService");
        Log.v(TAG, "Constructor of GeofenceIntentS - 1");
    }

    public void onCreate() {
        super.onCreate();
        Log.v(TAG, " GeofenceIntentS onCreate");
    }
    public void onDestroy() {
        super.onCreate();
        Log.v(TAG, " GeofenceIntentS onDestroy");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.v(TAG, "GeofenceIntentS = onHandleIntent");
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if( !geofencingEvent.hasError() ) {
            int transition = geofencingEvent.getGeofenceTransition();
            String notificationTitle = "Default";

            switch(transition) {
                case Geofence.GEOFENCE_TRANSITION_ENTER:
                    notificationTitle = "Geofence Entered";
                    Log.v(TAG, "Geofence Entered");
                    break;
                case Geofence.GEOFENCE_TRANSITION_EXIT:
                    notificationTitle = "Geofence Exited";
                    Log.v(TAG, "Geofence Entered");
                    break;
                case Geofence.GEOFENCE_TRANSITION_DWELL:
                    notificationTitle = "Geofence Dwell";
                    Log.v(TAG, "Geofence Entered");
                    break;
                default:
                    notificationTitle = "Geofence Unknown";
            }

            postGcm(notificationTitle);//Calls AsyncTask of sending GCM notification
            sendNotification(this, getTriggeringGeofences(intent), notificationTitle);//Notifies itself
        }
        else {
            Log.e(TAG, "GIS - Error in onHandle");
        }
    }

    /**
     * For GCM purpose
     * Called as an AsyncTask
     * @param title - used as title fro JSON object
     */
    public void postGcm(final String title){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                Log.v(TAG, "Sending POST to GCM");
                //System.out.println( "Sending POST to GCM" );
                String apiKey = "AIzaSyCUu4OrYUrZCxzJBCXk4goDQPfBqBVt1zw";
                //Content content = createContent();
                PostToGcm.post(apiKey, title);
                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.v(TAG, "Done the post fron async");
            }
        }.execute(null, null, null);
    }


    private void sendNotification( Context context, String notificationText, String notificationTitle ) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");

        wakeLock.acquire();
        Log.e(TAG, "Inside trying to send notification" + notificationText);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_media_play)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(false);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        wakeLock.release();

    }

    private String getTriggeringGeofences(Intent intent) {
        GeofencingEvent geofenceEvent = GeofencingEvent.fromIntent(intent);
        List<Geofence> geofences = geofenceEvent.getTriggeringGeofences();

        String[] geofenceIds = new String[geofences.size()];

        for( int i = 0; i < geofences.size(); i++) {
            geofenceIds[i] = geofences.get(i).getRequestId();
        }
        return TextUtils.join(", ", geofenceIds);
    }
}