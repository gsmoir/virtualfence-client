package com.wipro.wosggitlab.virtualfence;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by sam on 16-08-2015.
 * Intent Service to handle the message received during GCM notification, referred to by GcmBroadcastReceiver
 */
public class GcmMessageHandler extends IntentService{
    final String TAG = "Sam";
    String mes;
    private Handler handler;
    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        handler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent received in BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        mes = extras.getString("title");
        //showToast();
        //showDialog();

        showNotification(this, mes, "Geofence Alert");
        Log.i(TAG, "Received : (" + messageType + ")  " + extras.getString("title"));
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    /**
    public void showToast(){
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), mes, Toast.LENGTH_LONG).show();
            }
        });
    }
    **/

    private void showNotification( Context context, String notificationText, String notificationTitle ) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");

        wakeLock.acquire();

        Uri soundPath = Uri.parse("android.resource://com.wipro.wosggitlab.virtualfence/" + R.raw.notification_2);

        Log.e(TAG, "Inside show notification" + notificationText);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_media_play)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setSound(soundPath)
                //.setDefaults(Notification.DEFAULT_ALL)
                //.setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                .setAutoCancel(false);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        wakeLock.release();
    }
}