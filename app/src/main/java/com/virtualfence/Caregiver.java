package com.virtualfence;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.virtualfence.Constants.ServerValues;
import com.virtualfence.serverCodes.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * The Caregiver is the class to handle the layout of the Caregiver Home.
 * It also handles basic functionality to redirect to adding Cared devices.
 *
 * Performs registering to GCM server
 *
 * @author sam
 * @version 1.0
 * @since 2015-07-25
 */

public class Caregiver extends ActionBarActivity {

    //private MonitorRequestTask monitorRequestTask = null;

    private String TAG = "Sam";
    private GoogleCloudMessaging gcm;
    private String regId;
    private String PROJECT_NO = "648375628033";

    private Button button_setGeofence;
    private Button button_monitorRequest;
    //private  Button button_getGcmId;
    //private TextView textGcmId;
    //private EditText editTextEmail;

    private static final String USER_PREF = "UserDetails";
    private static final String DEFAULT = "N/A";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_caregiver);
            button_setGeofence = (Button) findViewById(R.id.button_add_cared);
            button_setGeofence.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.e(TAG, "Logging Button");
                    EditText editTextEmail = (EditText) findViewById(R.id.text_caredUserEmail);
                    String email = editTextEmail.getText().toString();
                    Log.v(TAG, "Entered email=" + email);
                    Intent caregiver_intent = new Intent(Caregiver.this, AddCared.class);
                    Log.v(TAG, "Putting Extra " + email);
                    caregiver_intent.putExtra("caredEmail", email);
                    startActivity(caregiver_intent);
                }
            });

            /*
            button_monitorRequest = (Button)findViewById(R.id.button_monitor_cared);
            button_monitorRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e(TAG,"Just Monitor button pressed");
                    EditText editTextEmail = (EditText) findViewById(R.id.text_caredUserEmail);
                    String email = editTextEmail.getText().toString();

                    requestMonitor(email);
                }
            });*/


        } catch (Exception e) {
            Log.v(TAG, "Exception", e);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_caregiver, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void requestMonitor(String email) {
    }

    /**
     * getRegId method to request theGCM registration ID fro GCM server
     * Need to add functionality to store in database, or some static final string constant
     */
    /*
    public void getRegId(){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regId = gcm.register(PROJECT_NO);
                    msg = "Device registered, registration ID=" + regId;
                    Log.v(TAG,  msg);

                } catch (IOException ex) {
                    msg = "Error = " + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                textGcmId = (TextView) findViewById(R.id.text_gcm_display);
                textGcmId.setText(msg);
            }
        }.execute(null, null, null);
    }*/

    public class MonitorRequestTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mGcmId;

        MonitorRequestTask(String email){
            mEmail = email;
            //Get GCM id from SharedPreferences
            SharedPreferences sharedPreferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
            mGcmId = sharedPreferences.getString("gcmId", DEFAULT);

        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                HashMap<String, String> hashedParams = new HashMap<String, String>();
                hashedParams.put("gcm_id", mGcmId);
                hashedParams.put("email", mEmail);
                ServerRequest serverRequest = new ServerRequest();

                JSONObject json = serverRequest.getJSONResponse(ServerValues.SERVER_IP+"/monitor", hashedParams);
                Log.v(TAG, "Calling request at"+ ServerValues.SERVER_IP+"/monitor with params " + hashedParams);

                if (json != null) {

                    String jsonString = json.getString("response");
                    Log.v(TAG, "Json not null and returns " + jsonString);

                    if (json.getBoolean("res")) {
                        finish();
                    }
                }
            } catch (JSONException e) {
                Log.v(TAG, "JSONException", e);
            } catch (Exception e) {
                Log.v(TAG, "Exception" , e);
            }

            return null;
        }
    }
}
