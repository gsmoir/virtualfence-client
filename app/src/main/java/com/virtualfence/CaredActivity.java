package com.virtualfence;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class CaredActivity extends ActionBarActivity implements OnCameraChangeListener {

    private static final String USER_PREF = "UserDetails";
    private static final String DEFAULT = "N/A";

    private GoogleMap map;
    ArrayList<Geofence> arraylist_geofences;
    ArrayList<LatLng> geoFenceCoordinates;
    ArrayList<Integer> geoRadius;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private int radius = 0;
    private Button button;

    private GeofenceStore geofenceStore;
    private final String TAG = "Sam";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cared);

        /**
         * Geofence
         */
        arraylist_geofences = new ArrayList<Geofence>();
        geoFenceCoordinates = new ArrayList<LatLng>();
        geoRadius = new ArrayList<Integer>();

        //Get fence details from SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        String sharedPrefLatitude = sharedPreferences.getString("latitude", "22.586385");
        String sharedPrefLongitude = sharedPreferences.getString("longitude", "88.42965949");
        String sharedPrefRadius = sharedPreferences.getString("radius", "40");

        latitude = Double.parseDouble(sharedPrefLatitude);
        longitude = Double.parseDouble(sharedPrefLongitude);
        radius = Integer.parseInt(sharedPrefRadius);

        //geoFenceCoordinates.add(new LatLng(22.586385, 88.429641));//Hard coded for now
        geoFenceCoordinates.add(new LatLng(latitude, longitude));
        geoRadius.add(radius);

        Log.e(TAG, "CaredAct - 6 =" + geoFenceCoordinates.get(0).latitude +" , "+ geoFenceCoordinates.get(0).longitude +" , " + geoRadius.get(0).intValue());
        //Setting up geofences
        arraylist_geofences.add(new Geofence.Builder().setRequestId("VirtualFenceID")
                        .setCircularRegion(geoFenceCoordinates.get(0).latitude, geoFenceCoordinates.get(0).longitude, geoRadius.get(0).intValue())
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setLoiteringDelay(3000)
                        .setTransitionTypes(
                                Geofence.GEOFENCE_TRANSITION_ENTER
                                        | Geofence.GEOFENCE_TRANSITION_DWELL
                                        | Geofence.GEOFENCE_TRANSITION_EXIT).build()
        );

        Log.e(TAG, "In the onCreate - after making arrayist_geofences");

        /**
         * Add the geofence to GeoStore
         */
        geofenceStore = new GeofenceStore(this, arraylist_geofences);
        Log.e(TAG, "After calling geofenceStore");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        //geofenceStore.disconnect();
        super.onStop();
    }

    @Override
    protected void onResume(){
        Log.e(TAG, "CA - onResume - 1");
        super.onResume();

        if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            setupMapifNeeded();
        }
        else {
            GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this), this, 0);
        }
    }

    private void setupMapifNeeded(){

        if (map == null) {
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.cared_map)).getMap();
            if(map != null) {
                setUpMap();
            }
        }
    }

    /**
     * Add markers or lines, add listeners or move the camera
     */

    private void setUpMap(){

        //Get fence details from SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        Log.v(TAG, "Checking if the details from SP "+ sharedPreferences.getString("latitude", "0.0")+" "+ sharedPreferences.getString("longitude", "0.0")+" "+ sharedPreferences.getString("radius", "0.0")+" ");

        Log.v(TAG, "SetupMap Lat, lon rad = " +latitude+" "+longitude+" "+radius+" ");

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), radius));
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        map.setIndoorEnabled(false);
        map.setMyLocationEnabled(true);
        map.setOnCameraChangeListener(this);
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        for(int i=0; i < geoFenceCoordinates.size(); i++) {
            map.addCircle(new CircleOptions().center(geoFenceCoordinates.get(i))
                    .radius(geoRadius.get(i).intValue())
                    .fillColor(Color.GREEN)
                    .strokeColor(Color.TRANSPARENT).strokeWidth(2));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cared, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}