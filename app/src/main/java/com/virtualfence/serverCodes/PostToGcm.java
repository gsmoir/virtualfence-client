package com.virtualfence.serverCodes;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by sam on 16-08-2015.
 *
 * PostToGcm class is basically meant for 3rd party server to trigger GCM notification through GCM server
 * Here, we write the server side code in the android code itself
 * It basically does call POST request to GCM server, using JSON object as the message
 */
public class PostToGcm {

    static final String TAG = "Sam";
    static ArrayList<String> regIds = new ArrayList<String>();

    /**
     * post method is the main method called from GeofenceIntentService to send the notification to intended receivers using GCM
     *
     * @param apiKey is needed for GCM Api
     * @param messageTitle is basic message type we are using to identify the event
     *                     May be, we can use message body later on
     */
    public static void post(String apiKey, String messageTitle){  // Content content){

        try{
            URL url = new URL("https://android.googleapis.com/gcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();//open connection
            conn.setRequestMethod("POST"); // Specify POST method
            conn.setRequestProperty("Content-Type", "application/json"); //Set the headers
            conn.setRequestProperty("Authorization", "key=" + apiKey);
            conn.setDoOutput(true);
            //String to work as JSON for GCM notification
            //String input = "{\"registration_ids\" : [\"APA91bE-JALeni13h5vmoeIZMSDsMTU8rAWnL3zg8rTKWpSKgSmwFmfM7TUiZ2_rZKIadnXItLz6E2cTeQj4dYRVVoivVsi3MOCtUXXU62GKHCPJSbZKSmOzaU0CZx5qaBSVHREIcQNrGu_py1s9ucE3owgSSlm9Gg\",\"APA91bGvMQ2WKXyI0DUnzTv0z2rYxsPjcwx-UU8pcSoOE256wfiRRcAZvur_otQciILYnQwj6Pdqbzoor9SMiflyLjw8eVSgdWLyk8vvTM2e0dtknR82grrPdkqKIgXLKJAI5ciOyY9WRBvRfZo2IKJiu_34XoEzqQ\"],\"data\" : {\"title\":\""+ messageTitle +"\",\"message\": \"Dummy body of message for now\"}}";
            //regIds.add("APA91bGvMQ2WKXyI0DUnzTv0z2rYxsPjcwx-UU8pcSoOE256wfiRRcAZvur_otQciILYnQwj6Pdqbzoor9SMiflyLjw8eVSgdWLyk8vvTM2e0dtknR82grrPdkqKIgXLKJAI5ciOyY9WRBvRfZo2IKJiu_34XoEzqQ");
            String input = "{\"registration_ids\" : [\"APA91bGvMQ2WKXyI0DUnzTv0z2rYxsPjcwx-UU8pcSoOE256wfiRRcAZvur_otQciILYnQwj6Pdqbzoor9SMiflyLjw8eVSgdWLyk8vvTM2e0dtknR82grrPdkqKIgXLKJAI5ciOyY9WRBvRfZo2IKJiu_34XoEzqQ\"],\"data\" : {\"title\":\""+ messageTitle +"\",\"message\": \"Dummy body of message for now\"}}";
            Log.v(TAG, input);
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());//To prepare request to server
            out.write(input);//write the JSON object string to server
            out.close();//Close the outputStreamWriter
            int responseCode = conn.getResponseCode();//Get the response
            Log.v(TAG, "\nSending 'POST' request to URL : " + url);
            Log.v(TAG, "Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));//Read the response
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();//Reading close

            /**
             * Tries for JSON, may be useful later on
             //Create JSONObject here
             JSONObject jsonParam = new JSONObject();
             jsonParam.put("title", "Testing title");
             jsonParam.put("message", message);
             //jsonParam.put("ID", "25");
             //jsonParam.put("description", "Real");
             //jsonParam.put("enable", "true");

             OutputStreamWriter out = new   OutputStreamWriter(conn.getOutputStream());
             out.write(jsonParam.toString());
             out.close();

             **/


            /**
             OutputStream os = conn.getOutputStream();
             os.write(input.getBytes());
             Log.v(TAG, "Post - 10");
             os.flush();
             **/

            Log.v(TAG, response.toString());

        } catch (MalformedURLException e) {
            Log.v(TAG, e.toString() + "MalformedURLException", e);
        } catch (IOException e) {
            Log.v(TAG, "IOException of P2G", e);
        }
        /**catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "JSONException caused here");
        }**/
    }
}