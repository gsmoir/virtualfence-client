package com.virtualfence.serverCodes;

/**
 * Created by sam on 01-10-2015.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class ServerRequest {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    static final String TAG = "Sam";

    static ArrayList<String> regIds = new ArrayList<String>();


    JSONObject jobj;

    public ServerRequest() {
        Log.v(TAG, "Inside ServerRequest class");
    }

    public JSONObject getJSONResponse(String urlStr, HashMap<String, String> params) {
        String responseString="";
        JSONObject response = null;
        try {
            Log.v(TAG, "Inside SR -  getJSONResponse, url = " + urlStr + " params = " + params);
            URL url = new URL(urlStr);
            Log.v(TAG, "Inside SR -  getJSONResponse - 1");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            Log.v(TAG, "Inside SR -  getJSONResponse - 2");
            conn.setRequestMethod("POST");
            conn.setReadTimeout(150000);
            conn.setConnectTimeout(150000);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            Log.v(TAG, "Inside SR -  getJSONResponse - 3");
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            Log.v(TAG, "Doing Post");
            out.write(getPostData(params));

            out.close();

            int responseCode = conn.getResponseCode();
            Log.v(TAG, "Collecting response, code = " + responseCode);
            if(responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                while((line = br.readLine()) != null ) {
                    Log.v(TAG, "String condition = " +responseString);
                    Log.v(TAG, "Line to write in string = " +line);
                    responseString += line;
                }
                br.close();
            } else {
                responseString = "";
            }

            Log.v(TAG, "ResponseString =  " + responseString);
            if(responseString!=null) {
                response = new JSONObject(responseString);
            }
        } catch (MalformedURLException e) {
            Log.v(TAG, "Exception", e);
        } catch (IOException e) {
            Log.v(TAG, "Exception", e);
        } catch (JSONException e) {
            Log.v(TAG, "Exception", e);
        }
        catch(Exception e){
            Log.v(TAG, "Exception", e);
        }
        Log.v(TAG, "ResponseString =  " + responseString);
        Log.v(TAG, "ResponseJSON =  " + response);
        return response;
    }

    /**
     * post method is the main method called from GeofenceIntentService to send the notification to intended receivers using GCM
     *
     * @param urlStr for route
     * @param params including message type we are using to identify the event, with message body
     */
    public JSONObject postNotification(String urlStr, HashMap<String, String> params){  // Content content){
        JSONObject response = null;
        try{
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();//open connection
            conn.setRequestMethod("POST"); // Specify POST method
            conn.setReadTimeout(150000);
            conn.setConnectTimeout(150000);
            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());//To prepare request to server
            out.write(getPostData(params));//Convert Post data to JSON
            out.close();//Close the outputStreamWriter

            int responseCode = conn.getResponseCode();//Get the response
            Log.v(TAG, "Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));//Read the response
            String inputLine;
            StringBuffer responseSb = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                responseSb.append(inputLine);
            }
            in.close();//Reading close
            Log.v(TAG, responseSb.toString());

            if(responseSb!=null) {
                response = new JSONObject(responseSb.toString());
            }


        } catch (MalformedURLException e) {
            Log.v(TAG, e.toString() + "MalformedURLException", e);
        } catch (IOException e) {
            Log.v(TAG, "IOException of P2G", e);
        } catch (JSONException e) {
            Log.v(TAG, "JSONException", e);
        }
        return response;
    }

    /**
     * Coverts HashMapped params to JSON compatible string
     * @param params
     * @return String in JSON compatible format
     */
    public String getPostData(HashMap<String, String> params) {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()) {
            if (first) {
                Log.v(TAG, "inside first, result =  " +result.toString());
                first = false;
            } else {
                result.append("&");
            }
            try {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                Log.v(TAG, "result =  " + result.toString());

            } catch (UnsupportedEncodingException e) {
                Log.v(TAG, "Exception", e);
            }
        }

        Log.v(TAG, " final result =  " + result.toString());
        return result.toString();
    }




    public JSONObject getJSONFromUrl(String url, List<NameValuePair> params) {
        try {
            Log.v(TAG, "Inside SR -  GetJSONFromURL, url = " +url +" params = " +params);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            Log.v(TAG, "Inside AsyncTask", e);
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.v(TAG, "Inside AsyncTask", e);
            e.printStackTrace();
        } catch (IOException e) {
            Log.v(TAG, "Inside AsyncTask", e);
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
            Log.e("JSON", json);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }


        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        return jObj;
    }


    public JSONObject getJSON(String url, List<NameValuePair> params) {
        Log.v(TAG, "Inside ServerRequest class - getJSON");

        Params param = new Params(url,params);
        Request myTask = new Request();
        try{
            Log.v(TAG, "Inside ServerRequest class - getJSON try- "+params );

            jobj= myTask.execute(param).get();
        }catch (InterruptedException e) {
            Log.v(TAG, "Exception", e);
        }catch (ExecutionException e){
            Log.v(TAG, "Inside ServerRequest class - inside catch", e);
            e.printStackTrace();
        }
        return jobj;
    }


    private static class Params {
        String url;
        List<NameValuePair> params;
        Params(String url, List<NameValuePair> params) {
            this.url = url;
            this.params = params;
        }
    }

    private class Request extends AsyncTask<Params, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(Params... args) {
            JSONObject json = null;
            try {
                Log.v(TAG, "Inside AsyncTask doInbackground"+args[0].url +" and " +args[0].params);
                ServerRequest request = new ServerRequest();
                json = request.getJSONFromUrl(args[0].url, args[0].params);
            }catch(Exception e){
                Log.v(TAG, "Inside AsyncTask", e);
            }
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                super.onPostExecute(json);
            }
            catch(Exception e){
                Log.v(TAG, "Inside AsyncTask", e);
            }

        }

    }
}