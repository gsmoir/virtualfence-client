package com.virtualfence;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

/**
 * GeofenceStore to enable geofencing as an Intent
 * Created by sam on 07-08-2015.
 */
public class GeofenceStore implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<Status>, LocationListener {

    private final String TAG = "Sam";//this.getClass().getSimpleName();
    private Context context;

    //Google API client object
    private GoogleApiClient googleApiClient;

    //Geofence Pending Intent
    private PendingIntent pendingIntent;

    //List of Geofences to monitor
    private ArrayList<Geofence> arraylist_geofences;

    //Geofence Request
    private GeofencingRequest geofencingRequest;

    //Request Object
    private LocationRequest locationRequest;

    /**
     * Constructor
     * @param ctx
     * @param geofences
     */
    public GeofenceStore(Context ctx, ArrayList<Geofence> geofences) {
        context = ctx;
        arraylist_geofences = new ArrayList<Geofence>(geofences);
        pendingIntent = null;

        Log.e(TAG, "GeofenceStore --- 1");
        /**
         * Build a GoogleApiClient, specifying the need for LocationServices, by adding the API to client
         * Specify connection callbacks, onConnectionFailed methods
         */
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Create Geofencing request
        geofencingRequest = new GeofencingRequest.Builder().addGeofences(arraylist_geofences).build();//Build the geofences
        pendingIntent = createRequestPendingIntent();//This fires up the IntentService which handles the geofence transitions

        //Submitting request to monitoring geofences
        PendingResult<Status> pendingResult = null;
        try {
            pendingResult = LocationServices.GeofencingApi.addGeofences(googleApiClient, geofencingRequest, pendingIntent);
            pendingResult.setResultCallback(this);

        } catch(SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            Log.e(TAG, "ACCESS_FINE_LOCATION exception caused here....", securityException);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(TAG, "GS - Suspended connection");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG, "GS - Location Information\n" +  "Provider:\t" + location.getProvider()
                + "Lat & Long:\t" + location.getLatitude() + ", " + location.getLongitude() + "\n");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v(TAG, "GS - Connection failed.");
    }

    @Override
    public void onResult(Status status) {
        if(status.isSuccess()) {
            Log.e(TAG, "GS - Geofencing success");
        } else if (status.isCanceled()) {
            Log.e(TAG, "GS - Canceled");
        } else if (status.isInterrupted()) {
            Log.e(TAG, "GS - Interrupted");
        }
    }

    /**
     * This creates a pendingIntent that is to be fired when geofence transitions takes place.
     * In this instance , we are using an InstanceService to handle the services.
     */
    private PendingIntent createRequestPendingIntent(){
        if(pendingIntent == null) {
            Intent intent = new Intent(context, GeofenceIntentService.class);
            pendingIntent = PendingIntent.getService(context,0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return pendingIntent;
    }
}
